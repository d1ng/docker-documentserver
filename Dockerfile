FROM cr.loongnix.cn/library/debian:buster-slim

LABEL maintainer="wangweijie@loongson.cn"

ARG ONLYOFFICE_VALUE=onlyoffice
ENV DEBIAN_FRONTEND=noninteractive PG_VERSION=11

#COPY deb/ /tmp

RUN echo "#!/bin/sh\nexit 0" > /usr/sbin/policy-rc.d && \
#    apt install -y /tmp/xserver-common_1.20.4-1.lnd.4_all.deb &&\
#    apt install -y /tmp/xvfb_1.20.4-1.lnd.4_loongarch64.deb &&\
#    apt-key add /usr/share/keyrings/debian-archive-buster-loongarch64-stable.gpg && \
    apt-get -y update && \
    apt-get -yq install wget apt-transport-https locales && \
    locale-gen en_US.UTF-8 && \
    echo ttf-mscorefonts-installer msttcorefonts/accepted-mscorefonts-eula select true | debconf-set-selections && \
    apt-get -yq install \
        adduser \
        apt-utils \
        bomstrip \
        certbot \
        curl \
        gconf-service \
        htop \
        libasound2 \
        libboost-regex-dev \
        libcairo2 \
        libcurl3-gnutls \
        libcurl4 \
        libgtk-3-0 \
        libnspr4 \
        libnss3 \
        libstdc++6 \
        libxml2 \
        libxss1 \
        libxtst6 \
        default-mysql-client \
        nano \
        net-tools \
        netcat-openbsd \
        nginx-extras \
        postgresql \
        postgresql-client \
        pwgen \
        rabbitmq-server \
        redis-server \
        software-properties-common \
        sudo \
        supervisor \
        xvfb \
        zlib1g \
	vim \
	php-fpm \
	php-curl && \
    echo "SERVER_ADDITIONAL_ERL_ARGS=\"+S 1:1\"" | tee -a /etc/rabbitmq/rabbitmq-env.conf && \
    sed -i "s/bind .*/bind 127.0.0.1/g" /etc/redis/redis.conf && \
    sed 's|\(application\/zip.*\)|\1\n    application\/wasm wasm;|' -i /etc/nginx/mime.types && \
    pg_conftool $PG_VERSION main set listen_addresses 'localhost' && \
    service postgresql restart && \
    sudo -u postgres psql -c "CREATE USER $ONLYOFFICE_VALUE WITH password '$ONLYOFFICE_VALUE';" && \
    sudo -u postgres psql -c "CREATE DATABASE $ONLYOFFICE_VALUE OWNER $ONLYOFFICE_VALUE;" && \ 
    service postgresql stop && \
    service redis-server stop && \
    service rabbitmq-server stop && \
    service supervisor stop && \
    service nginx stop && \
    rm -rf /var/lib/apt/lists/*

COPY config/supervisor/supervisor /etc/init.d/
COPY config/supervisor/ds/*.conf /etc/supervisor/conf.d/
COPY run-document-server.sh /app/ds/run-document-server.sh

EXPOSE 80 443

ARG COMPANY_NAME=onlyoffice
ARG PRODUCT_NAME=documentserver
ARG PRODUCT_EDITION=
ARG PACKAGE_VERSION=7.4.1-3
ARG TARGETARCH=loongarch

ENV COMPANY_NAME=$COMPANY_NAME \
    PRODUCT_NAME=$PRODUCT_NAME \
    PRODUCT_EDITION=$PRODUCT_EDITION \
    DS_DOCKER_INSTALLATION=true

RUN wget -q -P /tmp http://192.168.1.244/ttf-mscorefonts-installer_3.8_all.deb && \
    wget -q -P /tmp http://192.168.1.244/onlyoffice-documentserver_$PACKAGE_VERSION\_loongarch64.deb && \
    apt-get -y update && \
    apt-get -y install libbrotli1 && \
    service postgresql start && \
    apt-get -yq install /tmp/ttf-mscorefonts-installer_3.8_all.deb && \
    apt-get -yq install /tmp/onlyoffice-documentserver_$PACKAGE_VERSION\_loongarch64.deb && \
    service postgresql stop && \
    chmod 755 /etc/init.d/supervisor && \
    sed "s/COMPANY_NAME/${COMPANY_NAME}/g" -i /etc/supervisor/conf.d/*.conf && \
    service supervisor stop && \
    chmod 755 /app/ds/*.sh && \
    rm -f /tmp/*.deb && \
    rm -rf /var/log/$COMPANY_NAME && \
    rm -rf /var/lib/apt/lists/*

COPY nginx/ds-kod-include.conf /etc/nginx/includes/
COPY nginx/ds-kod-server.conf /etc/nginx/conf.d/

VOLUME /var/log/$COMPANY_NAME /var/lib/$COMPANY_NAME /var/www/$COMPANY_NAME/Data /var/lib/postgresql /var/lib/rabbitmq /var/lib/redis /usr/share/fonts/truetype/custom

ENTRYPOINT ["/app/ds/run-document-server.sh"]
